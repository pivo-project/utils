defmodule From.PushCommandTest do
  use ExUnit.Case
  doctest From.PushCommand

  test "build docker push command" do
    predictor_path = "test_path"
    expected_command = "docker"

    expected_arg_list = [
      "push",
      "registry.gitlab.com/pivo-project/predictor:from"
    ]

    {command, arg_list, path} = From.PushCommand.build(predictor_path)

    assert command == expected_command

    Enum.each(arg_list, fn arg -> assert Enum.member?(expected_arg_list, arg) end)

    assert predictor_path == path
  end
end
