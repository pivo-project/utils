defmodule From.BuildCommandTest do
  use ExUnit.Case
  doctest From.BuildCommand

  test "build docker build command" do
    predictor_path = "test_path"

    expected_command = "docker"

    expected_arg_list = [
      "build",
      "-t",
      "registry.gitlab.com/pivo-project/predictor:from",
      "-f",
      "#{Path.expand(predictor_path)}/from.Dockerfile",
      Path.expand(predictor_path)
    ]

    {command, arg_list, path} = From.BuildCommand.build(predictor_path)

    assert command == expected_command

    Enum.each(arg_list, fn arg -> assert Enum.member?(expected_arg_list, arg) end)

    assert path == predictor_path
  end
end
