defmodule Predict.CommandTest do
  use ExUnit.Case

  doctest Predict.Command

  test "build predict command" do
    predictor_path = "test_path"
    image_path = "test_path"

    expected_command = "docker"

    expected_arg_list = [
      "run",
      "--rm",
      "-v",
      "#{Path.expand(predictor_path)}/python:/app",
      "-v",
      "#{Path.expand(image_path)}:/image",
      "-w",
      "/app",
      "registry.gitlab.com/pivo-project/tensorflow:cpu",
      "python",
      "-c",
      "from src.predict import *; predict('/image')"
    ]

    {command, arg_list, path} = Predict.Command.build(predictor_path, image_path)

    assert command == expected_command

    Enum.each(arg_list, fn arg -> assert Enum.member?(expected_arg_list, arg) end)

    assert path == "#{Path.expand(predictor_path)}/python"
  end

end
