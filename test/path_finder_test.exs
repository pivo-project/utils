defmodule PathFinderTest do
  use ExUnit.Case
  doctest PathFinder

  test "expands path" do
    relative = "./.gitignore"

    {:ok, working_directory} = File.cwd()
    assert PathFinder.expand(relative) == "#{working_directory}/.gitignore"
  end
end