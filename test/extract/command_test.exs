defmodule Extract.CommandTest do
  use ExUnit.Case
  doctest Extract.Command

  test "build ffmpeg command" do
    src = "test_src"
    dest = "test_dest"

    expected_command = "ffmpeg"

    expected_arg_list = [
      "-i",
      Path.expand(src),
      "-r",
      "10/1",
      "-vf",
      "format=gray",
      "#{Path.expand(dest)}/%03d.jpeg"
    ]

    {command, arg_list} = Extract.Command.build(src, dest)

    assert command == expected_command

    Enum.each(arg_list, fn arg -> assert Enum.member?(expected_arg_list, arg) end)
  end
end
