defmodule ValidationTest do
  use ExUnit.Case

  doctest Validation

  test "passes with correct arguments" do
    src = "./.gitignore"
    dest = "./deps"
    func = fn (_, _) -> "SUCCESS" end

    assert Validation.run(src, dest, func) == "SUCCESS"
  end

  test "fails for non existent src" do
    src = "nothing"
    dest = "./temp"
    func = fn (_, _) -> "SUCCESS" end

    assert Validation.run(src, dest, func) == "Source does not exist"
  end

  test "fails for non directory dest" do
    src = "./.gitignore"
    dest = "./.gitignore"
    func = fn (_, _) -> "SUCCESS" end

    assert Validation.run(src, dest, func) == "Destination is not a directory"
  end
end
