defmodule Train.CommandTest do
  use ExUnit.Case

  doctest Train.Command

  test "build train command" do
    predictor_path = "test_path"
    categories_path = "test_path"

    expected_command = "docker"

    expected_arg_list = [
      "run",
      "--gpus",
      "all",
      "--rm",
      "-v",
      "#{Path.expand(predictor_path)}/python:/app",
      "-v",
      "#{Path.expand(categories_path)}:/categories",
      "-w",
      "/app",
      "tensorflow/tensorflow:2.11.0-gpu",
      "python",
      "-c",
      "from src.train_new_model import *; train_new_model('/categories')"
    ]

    {command, arg_list, path} = Train.Command.build(predictor_path, categories_path)

    assert command == expected_command

    Enum.each(arg_list, fn arg -> assert Enum.member?(expected_arg_list, arg) end)

    assert path == "#{Path.expand(predictor_path)}/python"
  end

end
