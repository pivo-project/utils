# Utils
Utility cli to be used by the Pivo Project

## Prerequisites
* `erlang`
* `elixir`
* `ffmpeg`

## Getting Started
* Clone this repository
* Run: `mix deps.get`
* Run: `mix escript.build`
* Run: `./utils <parameters>`

## Commands
Here's a list of the commands the cli's currently capable of running

### extract
This command takes in a video as its source, extracts frames from that video (at a rate of 100/1) and then 
move them to the destination folder

Usage: `./utils extract ../videos/carlsberg.MOV ../images`

### build_predictor_from
This command will build the _from_ Docker image for the Predictor Service and push it to the Gitlab Container Registry.

Usage: `./utils build_predictor_from ../predictor`

### train
This command will trigger a model retrain for the given categories path.

Usage: `./utils train ../predictor ../categories`

### predict 
This command will predict the category of a given image

Usage: `./utils predict ../predictor ../images/carlsberg.png`
