defmodule From do
  alias From.BuildCommand, as: Build
  alias From.PushCommand, as: Push

  def execute(path),
    do:
      Build.build(path)
      |> Shell.run()
      |> then(fn _ -> Push.build(path) end)
      |> Shell.run()
end
