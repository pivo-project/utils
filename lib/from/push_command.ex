defmodule From.PushCommand do
  @program "docker"
  @image_name "registry.gitlab.com/pivo-project/predictor:from"

  defp build_arg_list(),
  do: [
    "push",
    @image_name
  ]

  def build(predictor_path), do: {@program, build_arg_list(), predictor_path}
end
