defmodule From.BuildCommand do
  import PathFinder

  @program "docker"
  @image_name "registry.gitlab.com/pivo-project/predictor:from"
  @dockerfile_name "from.Dockerfile"

  defp build_full_dockerfile_location(path), do: expand("#{path}/#{@dockerfile_name}")

  defp build_arg_list(path),
    do: [
      "build",
      "-t",
      @image_name,
      "-f",
      build_full_dockerfile_location(path),
      expand(path)
    ]

  def build(predictor_path), do: {@program, build_arg_list(predictor_path), predictor_path}
end
