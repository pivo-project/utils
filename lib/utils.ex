defmodule Utils do
  use ExCLI.DSL, escript: true

  name "utils"
  description "Pivo Utils Cli"
  long_description "Utility cli used for the Pivo Project"

  command :extract do
    description "Extracts frames"
    long_description """
    Extracts frames from provided video source and outputs them to the destination folder
    """

    argument :src
    argument :dest

    run context do
      Validation.run(context[:src], context[:dest], &Extract.execute/2)
    end
  end

  command :build_predictor_from do
    description "Build Predictor from image"
    long_description """
    Builds the base image used by Predictor and pushes it to the gitlab container registry
    """

    argument :predictor_path

    run context do
      From.execute(context[:predictor_path])
    end
  end

  command :train do
    description "Train a new model"
    long_description """
    Trains a new model from a directory containing categories
    """

    argument :predictor_path
    argument :categories_path

    run context do
      Train.execute(context[:predictor_path], context[:categories_path])
    end
  end

  command :predict do
    description "Predict the category of an image"
    long_description """
    Loads the saved model and predicts the category of a provided image
    """

    argument :predictor_path
    argument :image_path

    run context do
      Predict.execute(context[:predictor_path], context[:image_path])
    end
  end
end
