defmodule Shell do
  def run({program, args, cd}) when is_bitstring(cd) do
    Task.async(fn -> System.cmd(program, args, into: IO.stream(), cd: cd) end)
    |> Task.await(:infinity)
  end

  def run({program, args}) do
    Task.async(fn -> System.cmd(program, args, into: IO.stream()) end)
    |> Task.await(:infinity)
  end

  def run({program, args, :capture}) do
    Task.async(fn -> System.cmd(program, args) end)
    |> Task.await(:infinity)
  end
end
