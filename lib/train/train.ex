defmodule Train do
  alias Train.Command, as: Command

  def execute(predictor_path, categories_path) do
      Train.RemoveExtra.execute(categories_path)

      Command.build(predictor_path, categories_path)
      |> Shell.run()
  end
end
