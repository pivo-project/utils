defmodule Train.Command do
  import PathFinder

  @program "docker"
  @image "tensorflow/tensorflow:2.11.0-gpu"

  defp build_python_path(predictor_path), do: expand("#{predictor_path}/python")

  defp build_import, do: "from src.train_new_model import *"

  defp build_call, do: "train_new_model('/categories')"

  defp build_code, do: "#{build_import()}; #{build_call()}"

  defp build_arg_list(predictor_path, categories_path),
    do: [
      "run",
      "--gpus",
      "all",
      "--rm",
      "-v",
      "#{expand(predictor_path)}/python:/app",
      "-v",
      "#{expand(categories_path)}:/categories",
      "-w",
      "/app",
      @image,
      "python",
      "-c",
      build_code()
    ]

  def build(predictor_path, categories_path),
    do:
      {@program, build_arg_list(predictor_path, categories_path),
       build_python_path(predictor_path)}
end
