defmodule Train.RemoveExtra do
  defp list(categories_path), do: Path.wildcard "#{categories_path}/*"

  defp get_image_count(category_path), do: Path.wildcard("#{category_path}/*")
  |> Enum.count()

  defp append_image_count(category_path), do: %{ "path" => category_path, "count" => get_image_count category_path }

  defp find_smallest_category(categories), do: Enum.min_by(categories, fn category -> Map.get(category, "count") end)

  defp should_remove?(smallest_count, file_name) do
    {file_index, _} = String.split(file_name, ".jpeg")
    |> Enum.at(0)
    |> String.split("/")
    |> List.last()
    |> Integer.parse()

    smallest_count < file_index
  end

  defp remove_extra(category, smallest_category) do
    Path.wildcard("#{Map.get(category, "path")}/*.jpeg")
    |> Enum.filter(fn file -> should_remove?(Map.get(smallest_category, "count"), file) end)
    |> Enum.map(&File.rm/1)
  end

  def execute(categories_path) do
    categories = list(categories_path)
    |> Enum.map(&append_image_count/1)

    smallest_category = find_smallest_category(categories)

    categories
    |> Enum.filter(fn category -> Map.get(category, "count") != Map.get(smallest_category, "count") end)
    |> Enum.map(fn category -> remove_extra(category, smallest_category) end)
  end
end
