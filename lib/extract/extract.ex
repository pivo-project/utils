defmodule Extract do
  def execute(src, dest),
    do:
      Extract.Command.build(src, dest)
      |> Shell.run()
      |> then(fn _ -> Extract.Resize.execute(dest) end)
end
