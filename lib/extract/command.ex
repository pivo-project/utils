defmodule Extract.Command do
  import PathFinder

  @program "ffmpeg"
  @ratio "10/1"
  @filename_pattern "%03d.jpeg"

  defp build_full_dest(dest), do: expand("#{dest}/#{@filename_pattern}")

  defp build_arg_list(src, dest),
    do: [
      "-i",
      expand(src),
      "-r",
      @ratio,
      "-vf",
      "format=gray",
      build_full_dest(dest)
    ]

  def build(src, dest), do: {@program, build_arg_list(src, dest)}
end
