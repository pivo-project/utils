defmodule Extract.Resize do
  defp resize_image(image_path),
    do:
      Mogrify.open(image_path)
      |> Mogrify.resize_to_limit("101x180")
      |> Mogrify.save(path: image_path, in_place: true)

  def execute(destination),
    do:
      Path.wildcard("#{destination}/*.jpeg")
      |> Enum.each(&resize_image/1)
end
