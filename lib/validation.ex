defmodule Validation do
  defp src_exists?(src), do: if File.exists?(src), do: :ok, else: :src_non_existent

  defp dest_is_dir?(dest), do: if File.dir?(dest), do: :ok, else: :dest_not_dir

  defp error_message(:src_non_existent), do: "Source does not exist"

  defp error_message(:dest_not_dir), do: "Destination is not a directory"

  def run(src, dest, func) do
    with :ok <- src_exists?(src),
         :ok <- dest_is_dir?(dest) do
      func.(src, dest)
    else
      err -> error_message(err)
    end
  end
end
