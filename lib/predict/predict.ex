defmodule Predict do
  alias Predict.Command, as: Command

  def execute(predictor_path, image_path),
    do:
      Command.build(predictor_path, image_path)
      |> Shell.run()
end
