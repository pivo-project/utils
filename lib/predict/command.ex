defmodule Predict.Command do
  import PathFinder

  @program "docker"
  @image "registry.gitlab.com/pivo-project/tensorflow:cpu"

  defp build_python_path(predictor_path), do: expand("#{predictor_path}/python")

  defp build_import, do: "from src.predict import *"

  defp build_call(), do: "predict('/image')"

  defp build_code(), do: "#{build_import()}; #{build_call()}"

  defp build_arg_list(predictor_path, image_path),
    do: [
      "run",
      "--rm",
      "-v",
      "#{expand(predictor_path)}/python:/app",
      "-v",
      "#{expand(image_path)}:/image",
      "-w",
      "/app",
      @image,
      "python",
      "-c",
      build_code()
    ]

  def build(predictor_path, image_path),
    do: {@program, build_arg_list(predictor_path, image_path), build_python_path(predictor_path)}
end
