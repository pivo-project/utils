defmodule PathFinder do
  def expand(relative), do: Path.expand(relative)
end
